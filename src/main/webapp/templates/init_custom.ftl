<#assign
show_footer = getterUtil.getBoolean(themeDisplay.getThemeSetting("show-footer"))
show_header = getterUtil.getBoolean(themeDisplay.getThemeSetting("show-header"))
show_header_search = getterUtil.getBoolean(themeDisplay.getThemeSetting("show-header-search"))
wrap_content = getterUtil.getBoolean(themeDisplay.getThemeSetting("wrap-content"))
/>

<#if wrap_content>
	<#assign portal_content_css_class = "container" />
<#else>
	<#assign portal_content_css_class = "" />
</#if>

<#--checking general roles-->
<#assign show_header_search = getterUtil.getBoolean(themeDisplay.getThemeSetting("show-header-search")) />
<#assign userRoles = user.getRoles()/>
<#assign enableControlMenu = false/>
<#list userRoles as role>
	<#if role.getName() == "Administrator"
	|| role.getName() == "Portal Content Reviewer"
	|| role.getName() == "Owner">
		<#assign enableControlMenu = true/>
	</#if>
</#list>